class Locators:

    # Login elements (Login page)
    login_textbox_id = "user[email]"
    password_textbox_id = "user[password]"
    login_button_id = "sign-in"
    linked_in_btn_xpath = '//*[@id="main-content"]/div/div/article/div/ul/li/a/i'
    registration_link_xpath = '//*[@id="main-content"]/div/div/aside/a'
    remember_me_id = 'user[remember_me]'

    # Footer
    home_xpath = '/html/body/footer/div/article/ul/li[1]/a'
    all_courses_xpath = '/html/body/footer/div/article/ul/li[2]/a'

    # support_xpath = '//*[@id="global-footer"]/div[1]/div/div/ul/li[2]/a'
    # pr_policy_xpath = '//*[@id="global-footer"]/div[1]/div/div/ul/li[3]/a'

    # Header
    logo_link_xpath = '/html/body/header/div/div/section[2]/a'
    sign_in_xpath = '/html/body/header/div/div/section[1]/ul/li/a'
    sign_out_xpath = '/html/body/header/div/div/section[1]/ul/li/ul/li[4]/a'




    # New user registration page
    first_name_id = "user[first_name]"

    # Logged In
    logged_in_container_id = 'notifications'
    logged_in_class_name = 'message-text'

    # Alerts
    wrong_credentials_msg_id_xpath = '//*[@id="notice"]/ul/li'
