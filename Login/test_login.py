import allure
import unittest
from selenium import webdriver
from allure_commons.types import AttachmentType
from Pages.loginPage import LoginPage
from Pages.userCreatePage import UserCreatePage
from Locators.locators import Locators


class TestLoginPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.page = LoginPage(self.driver)
        self.page_reg = UserCreatePage(self.driver)

    def tearDown(self):
        self.driver.quit()

    @allure.feature('Open Page')
    @allure.story('Testing for unregistered users')
    @allure.severity('critical')
    def test_login_page_not_registered(self):
        # Testing for unregistered users
        self.driver.get("https://courses.ultimateqa.com/users/sign_in")
        if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
            self.page.logging_in("test@test.com", "pass")
            self.assertTrue(self.page.is_alert_check(), 'Error!!! Valid Random Credentials!')
        else:
            print('Not the e-mail field has to test additionally')

    @allure.feature('Open Page')
    @allure.story('Testing for unregistered users with not an email type login')
    @allure.severity('critical')
    def test_login_page_not_registered(self):
        # Testing for unregistered users
        self.driver.get("https://courses.ultimateqa.com/users/sign_in")
        if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
            self.page.logging_in("test@test.com", "pass")
            self.assertTrue(self.page.is_alert_check(), 'Error!!! Valid Random Credentials!')
        else:
            print('Not the e-mail field has to test additionally')

    @allure.feature('Open Page')
    @allure.story('Testing for registered users')
    @allure.severity('critical')
    def test_login_page_registered(self):
        # Testing for registered users

        self.driver.get("https://courses.ultimateqa.com/users/sign_in")

        if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
            self.page.logging_in()
            with allure.step("Snapshot"):
                allure.attach(self.driver.get_screenshot_as_png(),
                              name='snapshot', attachment_type=AttachmentType.PNG)
            self.assertTrue(self.page.is_logged_check_by_msg(), 'Error!!! InValid Credentials!')
        else:
            print('Not the e-mail field has to test additionally')

    @allure.feature('Open Page')
    @allure.story('Testing for registration with LinkedIn without logging')
    @allure.severity('minor')
    def test_login_with_linkedIn(self):
        # Testing for registration with LinkedIn without logging

        self.driver.get("https://courses.ultimateqa.com/users/sign_in")
        self.page.click_linked_in_btn()
        with allure.step("Snapshot with LinkedIn"):
            allure.attach(self.driver.get_screenshot_as_png(),
                          name='snapshot', attachment_type=AttachmentType.PNG)
        self.assertTrue('LinkedIn' in self.driver.title, 'LinkedIn reference FAIL')

    @allure.feature('Open Page')
    @allure.story('Testing registration link')
    @allure.severity('trivial')
    def test_registration(self):
        # Testing registration link

        self.driver.get("https://courses.ultimateqa.com/users/sign_in")
        self.page.click_registration()
        self.assertTrue(self.page_reg.is_first_name_present(), "Not in the registration page!")

    @allure.feature('Open Page')
    @allure.story('Testing Header and Footer links')
    @allure.severity('trivial')
    def test_footer_header_links(self):
        # Testing Header and Footer links
        self.driver.get("https://courses.ultimateqa.com/users/sign_in")

        # Testing header links
        self.assertTrue(self.page.item_from_header(), "Wrong Header Links!")
        # Testing footer links
        self.assertTrue(self.page.item_from_footer(), "Wrong Footer Links!")

    @allure.feature('Open Page')
    @allure.story('Testing remember me checkbox (Have appropriate cookies been created?)')
    @allure.severity('normal')
    def test_remember_checkbox(self):
        # Testing Remember me checkbox
        # Is creating an appropriate cookie with a checkbox?

        self.driver.get("https://courses.ultimateqa.com/users/sign_in")

        # Is signed in already?
        if not self.page.is_signed_in_already():
            # If isn't signed in, signs in
            if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
                self.page.click_checkbox()
                self.page.logging_in()
                with allure.step("Logging in"):
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name='snapshot', attachment_type=AttachmentType.PNG)
                self.assertTrue(self.page.is_logged_check_by_msg(), 'Error!!! InValid Credentials!')
                self.assertTrue(self.page.check_cookies(), 'Checkbox "Remember me" is not working')
            else:
                print('Not the e-mail field has to test additionally')
        else:
            self.page.click_sign_out()
            if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
                self.page.click_checkbox()
                self.page.logging_in()
                self.assertTrue(self.page.is_logged_check_by_msg(), 'Error!!! InValid Credentials!')
                self.assertTrue(self.page.check_cookies(), 'Checkbox "Remember me" is not working')

    @allure.feature('Open Page')
    @allure.story('Testing remember me checkbox (Have appropriate cookies been created without a checkbox?)')
    @allure.severity('normal')
    def test_remember_checkbox_not_checked(self):
        # Testing Remember me checkbox.
        # Is creating an appropriate cookie without a checkbox?
        self.driver.get("https://courses.ultimateqa.com/users/sign_in")

        # Is signed in already?
        if not self.page.is_signed_in_already():
            # If isn't signed in, signs in
            if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
                self.page.logging_in()
                self.assertTrue(self.page.is_logged_check_by_msg(), 'Error!!! InValid Credentials!')
                self.assertTrue(not self.page.check_cookies(), 'Checkbox "Remember me" is not working')
            else:
                print('Not the e-mail field has to test additionally')
        else:
            self.page.click_sign_out()
            if self.page.get_element_attribute(Locators.login_textbox_id) == 'email':
                self.page.logging_in()
                self.assertTrue(self.page.is_logged_check_by_msg(), 'Error!!! InValid Credentials!')
                self.assertTrue(not self.page.check_cookies(), 'Checkbox "Remember me" is not working')


if __name__ == '__main__':
    unittest.main()




