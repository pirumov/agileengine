from Locators.locators import Locators


class UserCreatePage:

    def __init__(self, driver):
        self.driver = driver

    def is_first_name_present(self):
        return self.driver.find_element_by_id(Locators.first_name_id).size != 0
