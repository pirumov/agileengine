from Locators.locators import Locators
from selenium.common.exceptions import NoSuchElementException


class LoginPage:

    def __init__(self, driver):
        self.driver = driver

    def input_login(self, login):
        self.driver.find_element_by_id(Locators.login_textbox_id).clear()
        self.driver.find_element_by_id(Locators.login_textbox_id).send_keys(login)

    def input_password(self, password):
        self.driver.find_element_by_id(Locators.password_textbox_id).clear()
        self.driver.find_element_by_id(Locators.password_textbox_id).send_keys(password)

    def click_linked_in_btn(self):
        self.driver.find_element_by_xpath(Locators.linked_in_btn_xpath).click()

    def logging_in(self, login="test_test@test.com", password="qwertyytrewq"):
        self.input_login(login)
        self.input_password(password)
        self.click_sign_in()

    def click_registration(self):
        self.driver.find_element_by_xpath(Locators.registration_link_xpath).click()

    def click_sign_in(self):
        self.driver.find_element_by_id(Locators.login_button_id).click()

    def click_sign_out(self):
        self.driver.find_element_by_xpath(Locators.sign_out_xpath).click()

    def click_checkbox(self):
        self.driver.find_element_by_id(Locators.remember_me_id).click()

    def is_signed_in_already(self):
        try:
            self.driver.find_element_by_xpath(Locators.sign_out_xpath)
            return True
        except NoSuchElementException:
            return False

    def item_from_footer(self):
        is_true = True

        if self.driver.find_element_by_xpath(Locators.home_xpath).get_attribute("href") \
                != "https://courses.ultimateqa.com/":
            is_true = is_true and False
        elif self.driver.find_element_by_xpath(Locators.all_courses_xpath).get_attribute("href") \
                != "https://courses.ultimateqa.com/collections":
            is_true = is_true and False
        else:
            is_true = is_true and True
        return is_true

    def item_from_header(self):
        is_true = True

        if self.driver.find_element_by_xpath(Locators.logo_link_xpath).get_attribute("href") \
                != "https://courses.ultimateqa.com/collections":
            is_true = is_true and False
        elif self.driver.find_element_by_xpath(Locators.sign_in_xpath).get_attribute("href") \
                != "https://courses.ultimateqa.com/users/sign_in":
            is_true = is_true and False
        else:
            is_true = is_true and True
        return is_true

    def is_alert_check(self):
        alert_msg = self.driver.find_element_by_xpath(Locators.wrong_credentials_msg_id_xpath).text
        if alert_msg == 'Invalid Email or password.':
            return True
        else:
            return False

    def is_logged_check_by_msg(self):
        self.driver.execute_script("arguments[0].style.display = 'block';",
                                   self.driver.find_element_by_id(Locators.logged_in_container_id))
        alert_msg = self.driver.find_element_by_class_name(Locators.logged_in_class_name).text
        if alert_msg == 'Signed in successfully.':
            return True
        else:
            return False

    def get_element_attribute(self, locator, locator_type='id', attribute_name='type'):
        find_by = {
            'id'          : lambda x, y: self.driver.find_element_by_id(x).get_attribute(y),
            'xpath'       : lambda x, y: self.driver.find_element_by_xpath(x).get_attribute(y),
            'css_selector': lambda x, y: self.driver.find_element_by_css_selector(x).get_attribute(y)
        }

        return find_by[locator_type](locator, attribute_name)

    def check_cookies(self, name='remember_user_token'):
        cookies = self.driver.get_cookies()

        for cookie in cookies:
            if cookie['name'] != name:
                continue
            else:
                return True
        else:
            return False
